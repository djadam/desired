ActiveAdmin.register DesiredFirst do
  actions :index
  index do
    selectable_column
    id_column
    column :application_id
    column :app_identifier
    column :amount
    column :razorpay_order_id

    column :payment_gateway
    column :payment_completed_at
    column :ulb_name
    column :authority

    column :district
    column :revised_fee
    column :payment_status
    column :app_status
    actions
  end

  filter :application_id
  filter :app_identifier
  filter :amount
  filter :razorpay_order_id
  filter :payment_gateway
  filter :payment_completed_at
  filter :ulb_name
  filter :authority
  filter :district
  filter :revised_fee
  filter :payment_status
  filter :app_status
end