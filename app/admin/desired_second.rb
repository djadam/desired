ActiveAdmin.register DesiredSecond do
  actions :index
  index do
    selectable_column
    id_column
    column :application_id
    column :app_identifier
    column :ulb_name
    column :authority
    column :district
    column :revised_fee
    actions
  end

  filter :application_id
  filter :app_identifier
  filter :ulb_name
  filter :authority
  filter :district
  filter :revised_fee
end