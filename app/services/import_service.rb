class ImportService
  def initialize
    @file = 'lib/data.xlsx'
  end

  def import
    data = Roo::Spreadsheet.open(@file)
    headers = data.row(1)
    data.each_with_index do |row, idx|
      next if idx == 0
      user_data = Hash[[headers, row].transpose]
      
      DesiredFirst.create(application_id: user_data['application_id'], app_identifier: user_data['application_identifier'], amount: user_data['amount'], razorpay_order_id: user_data['razorpay_order_id'], payment_gateway: user_data['payment_method'], payment_completed_at: user_data['payment_completed_at'], ulb_name: user_data['ulb_name'], district: user_data['district'], revised_fee: user_data['revised_fee'], payment_status: user_data['payment_status'], app_status: user_data['status'], authority: user_data['district_administration_area'], payment_id: user_data['id'])

      DesiredSecond.create(application_id: user_data['application_id'], app_identifier: user_data['application_identifier'], ulb_name: user_data['ulb_name'], district: user_data['district'], revised_fee: user_data['revised_fee'], authority: user_data['district_administration_area'])
    end
  end
end