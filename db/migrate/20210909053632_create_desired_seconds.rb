class CreateDesiredSeconds < ActiveRecord::Migration[6.0]
  def change
    create_table :desired_seconds do |t|
      t.integer :application_id
      t.string  :app_identifier
      t.string  :ulb_name
      t.string  :authority
      t.string  :district
      t.boolean :revised_fee, default: false
      t.timestamps
    end
  end
end
