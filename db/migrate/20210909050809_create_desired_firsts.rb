class CreateDesiredFirsts < ActiveRecord::Migration[6.0]
  def change
    create_table :desired_firsts do |t|
      t.integer :application_id
      t.integer :payment_id
      t.string  :app_identifier
      t.float   :amount
      t.string  :razorpay_order_id
      t.string  :payment_gateway
      t.string  :payment_completed_at
      t.string  :ulb_name
      t.string  :authority
      t.string  :district
      t.boolean :revised_fee, default: false
      t.string :payment_status
      t.string :app_status
      t.timestamps
    end
  end
end
